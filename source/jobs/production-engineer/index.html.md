---
layout: job_page
title: "Production Engineer"
---

A production engineer is a developer with deep knowledge of some parts of the
stack, whether it be networking, or the Linux kernel, or even a specific
interest in scaling and algorithms.

It could also be seen as any systems engineer who aims to code themselves out of
a job by automating all the things.

## Responsibilities

* As a Production Engineer you will be on a PagerDuty rotation to respond to
  GitLab.com availability incidents and provide support for service engineers
  with customer incidents.
* Improve GitLab.com availability and performance.
* Incident response for GitLab.com.
* Making GitLab easier to maintain for administrators all over the world.
* Manage our infrastructure with Chef (and a little Ansible).
* Build monitoring systems so alerts trigger on symptoms and not on outages.
* Improve the deployment process to make it as boring as possible.
* Scaling GitLab to support hundred of thousands of concurrent users.
* Work comfortably at any level of the stack.

## Workflow

* You work on issues in the [infrastructure repository](https://gitlab.com/gitlab-com/infrastructure/issues).
* The priority of the issues can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).

## Requirements for Applicants

* Linux depth of knowledge (we use Ubuntu Server).
* Database scaling depth of knowledge (we use PostgreSQL and Redis).
* Chef experience (optional if you are awesome on something else).
* Ruby scripting experience (our preferred language for operations scripts).
* Programming expertise (Ruby and Ruby on Rails preferred; for GitLab
  debugging).
* Collaborative team spirit with great communication skills.
* Proactive, go-for-it attitude. When you see something broken, you can't help
  but fix it.
* You share our [values](/handbook/#values), and work in accordance with those
  values.

## Hiring Process


Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).


* Qualified applicants receive a short questionnaire and coding exercise from our Global Recruiters
* The review process for this role can take a little longer than usual but if in doubt, check in with the Global recruiter at any point.
* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first 45 minute behavioral interview with the Infrastructure Lead
* Candidates will then be invited to schedule a 45 minute technical interview with a Production Engineer
* Candidates will be invited to schedule a third interview with our VP of Engineering
* Finally, candidates will have a 50 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email


Additional details about our process can be found on our [hiring page](/handbook/hiring).
