---
layout: comparison_page
title: GitLab compared to other tools
suppress_header: true
image_title: '/images/comparison/title_image.png'
extra_css:
  - compared.css
---

## GitLab.com vs. GitHub.com
GitLab.com is the GitLab Enterprise Edition instance ran by GitLab Inc.
It's a public instance where anyone can create projects for free.
In that sense, it competes with GitHub.com.

### Everything mentioned in <a href='/comparison/gitlab-vs-github.html'>GitLab versus GitHub</a>

### Unlimited public private repositories and unlimited collaborators for free
No matter the type of project or how many people you collaborate with,
you can use GitLab.com for free. That means free public projects,
free private projects and as many people on those as you need.

### GitLab.com runs GitLab Enterprise Edition with all its features.
All the features from GitLab Enterprise Edition run on GitLab.com.
This means you can use Push Rules, Merge Request Approvals and even
File Locking on GitLab.com.

### Free CI with Shared or Own Runner
GitLab.com has shared runners that allow you to use GitLab CI completely
free. Alternatively, you can set up your own Runner for faster build processing
or special requirements.

### GitHub.com has faster pageloads than GitLab.com
Improving this is a [work in progress](https://gitlab.com/gitlab-com/operations/issues/42/).

[Read more about GitLab CI](/gitlab-ci)
